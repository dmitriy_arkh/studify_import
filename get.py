import datetime

import requests
from bs4 import BeautifulSoup

import constants as c


def get_timetable(group_name):
    datetime_now = datetime.datetime.now()
    year = datetime_now.strftime('%Y')
    semester = 1 if datetime_now.month in c.FIRST_S_MONTHS else 2
    response = requests.get(c.TIMETABLE_GROUP_TEMPLATE % (year, semester, group_name))
    timetable = []
    assert response.status_code == 200
    soup = BeautifulSoup(response.text, 'lxml')
    grp, date = soup.find('h3').text.strip().split('"')[1:]
    assert grp != 'False'
    weeks = soup.find_all("div", attrs={'class': 'tab-pane'})[1:3]
    for week in weeks:
        week_number = int(week['id'].split('_')[1])-1


        days = week.find_all("div", attrs={'class': 'day'})
        for day in days:
            name_of_day = day.find('div', {"class": 'header'}).find('div').text.strip()
            day_number = c.WEEK_DAYS[name_of_day.lower()]
            pairs = day.find_all('div', attrs={"class": 'line'})
            for pair in pairs:
                start_time = datetime.datetime.strptime(
                    pair.find('div', attrs={'class': 'time'}).find('div').text.strip(),
                    '%H:%M'
                )
                end_time = start_time + datetime.timedelta(minutes=c.PAIR_LEN_MINUTES)
                discipline = pair.find('div', attrs={'class': 'discipline'})
                subgroups = discipline.find_all('ul')
                for subgroup in subgroups:
                    pair_name = subgroup.find('span', attrs={'class': 'name'}).text.strip().lower().capitalize()
                    lis = [x.text.strip() for x in subgroup.find_all('li')]
                    pair_type, pair_tutor, pair_room = lis[-3:]
                    pair_type = pair_type.split('(')[-1][:-1].lower().capitalize()
                    timetable.append({
                        'week_number': week_number,
                        'day_number': day_number,
                        'start_time': start_time,
                        'end_time': end_time,
                        'pair_name': pair_name,
                        'pair_type': pair_type,
                        'pair_room': pair_room,
                        'pair_tutor': pair_tutor

                    })
    return timetable
