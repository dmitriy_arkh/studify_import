from get import get_timetable
from post import *

if __name__ == '__main__':
    group = input('Введите название группы (например, "БИС15-01" [БЕЗ КАВЫЧЕК])\n:> ')

    timetable = get_timetable(group)
    driver = get_driver()
    login(driver)
    while timetable:
        pair = timetable.pop()
        try:
            add_pair(driver, pair)
        except WebDriverException:
            timetable.append(pair)
    print()
