import os
from time import sleep

from selenium.common.exceptions import WebDriverException
from selenium.webdriver import Chrome, Firefox
from selenium.webdriver.common.keys import Keys

EDITOR_URL = 'http://editor.rvuzov.ru'

DRIVER_CLASS = Chrome


def get_driver() -> DRIVER_CLASS:
    geckodriver_path = '.\\geckodriver.exe' if os.name == 'nt' else 'geckodriver'
    chromedriver_path = '.\\chromedriver.exe' if os.name == 'nt' else 'chromedriver'

    try:
        driver = Firefox(executable_path=geckodriver_path)
    except:
        driver = Chrome(executable_path=chromedriver_path)
    driver.implicitly_wait(2)
    driver.maximize_window()

    return driver


def login(driver: DRIVER_CLASS):
    driver.get(EDITOR_URL)

    input("Выполните вход. При необходимости заполните данные ВУЗ'а/группы."
          "\nКогда откроется таблица расписания,"
          " нажмите ENTER\n> ")


def add_pair(driver: DRIVER_CLASS, pair: dict):
    try:
        driver.find_element_by_xpath('//button/span[text()="Закрыть"]/..').click()
    except WebDriverException:
        pass

    driver.find_element_by_xpath('//button[@ng-click="calCtrl.addLesson($event, -1, calCtrl.weekDate)"]').click()

    inputs = driver.find_element_by_class_name('lesson-modal-form__column').find_elements_by_xpath('.//input')
    inputs[0].send_keys(pair['pair_name'])
    inputs[1].send_keys(pair['pair_type'])
    inputs[1].send_keys(Keys.ARROW_DOWN)
    inputs[1].send_keys(Keys.ENTER)
    for _ in range(4):
        inputs[2].send_keys(Keys.BACK_SPACE)
    inputs[2].send_keys(pair['start_time'].strftime('%H%M'))
    for _ in range(4):
        inputs[3].send_keys(Keys.BACK_SPACE)
    inputs[3].send_keys(pair['end_time'].strftime('%H%M'))
    inputs[4].send_keys(pair['pair_tutor'])
    inputs[5].send_keys(pair['pair_room'])

    driver.find_element_by_xpath('//md-tab-item/span[text()="Периодически"]/..').click()
    sleep(1)
    if pair['day_number'] == 0:
        driver.find_element_by_xpath('//button/span[text()="пн"]/..').click()
    elif pair['day_number'] == 1:
        driver.find_element_by_xpath('//button/span[text()="вт"]/..').click()
    elif pair['day_number'] == 2:
        driver.find_element_by_xpath('//button/span[text()="ср"]/..').click()
    elif pair['day_number'] == 3:
        driver.find_element_by_xpath('//button/span[text()="чт"]/..').click()
    elif pair['day_number'] == 4:
        driver.find_element_by_xpath('//button/span[text()="пт"]/..').click()
    elif pair['day_number'] == 5:
        driver.find_element_by_xpath('//button/span[text()="сб"]/..').click()
    elif pair['day_number'] == 6:
        driver.find_element_by_xpath('//button/span[text()="вс"]/..').click()
    if pair['week_number']:
        driver.find_element_by_xpath('//button/span[text()="четная"]/..').click()
    else:
        driver.find_element_by_xpath('//button/span[text()="нечетная"]/..').click()
    print()
    sleep(2)
    driver.find_element_by_xpath('//button[@ng-click="lmfCtrl.save()"]').click()
    sleep(2)
