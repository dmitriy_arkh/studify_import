WEEK_DAYS = {
    k: i for i, k in enumerate(['понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота', 'воскресенье'])
}
PAIR_LEN_MINUTES = 90
FIRST_S_MONTHS = [6,7,8,9,10,11,12]
TIMETABLE_GROUP_TEMPLATE = 'https://timetable.pallada.sibsau.ru/timetable/group/%s/%s/%s'

