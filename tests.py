import unittest
from get import get_timetable


class InTests(unittest.TestCase):
    def test_get_timetable_returns_list(self):
        result = get_timetable('БИС15-01')
        assert isinstance(result, list)

    def test_get_timetable_raises_on_non_existent_group(self):
        with self.assertRaises(AssertionError):
            get_timetable('i hope it does not exist')

    def test_get_timetable_returns_list_non_empty_for_existing_group(self):
        result = get_timetable('БИС15-01')
        assert result != []


if __name__ == '__main__':
    unittest.main()
